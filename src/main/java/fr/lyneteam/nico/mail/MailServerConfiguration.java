package fr.lyneteam.nico.mail;

public class MailServerConfiguration {
	private String smtphost, smtpuser, smtppassword, pophost, popuser, poppassword, name, user, text;
	private int smtpport, popport;
	
	public MailServerConfiguration(String smtphost, String smtpuser, String smtppassword, String pophost, String popuser, String poppassword, String name, String user, String text, int smtpport, int popport) {
		this.smtphost = smtphost;
		this.smtpuser = smtpuser;
		this.smtppassword = smtppassword;
		this.pophost = pophost;
		this.popuser = popuser;
		this.poppassword = poppassword;
		this.name = name;
		this.user = user;
		this.text = text;
		this.smtpport = smtpport;
		this.popport = popport;
	}
	
	public MailServerConfiguration(String host, String name, String user, String password, String text) {
		this(host, user, password, host, user, password, name, user, text, 25, 587);
	}
	
	public MailServerConfiguration(String host, String name, String user, String password) {
		this(host, name, user, password, "Could not show the content of this message because your brooser doesn't accept the HTML format.");
	}

	public final String getSMTPHost() {
		return this.smtphost;
	}
	
	public final String getSMTPUser() {
		return this.smtpuser;
	}
	
	public final String getSMTPPassword() {
		return this.smtppassword;
	}
	
	public final String getPOPHost() {
		return this.pophost;
	}
	
	public final String getPOPUser() {
		return this.popuser;
	}
	
	public final String getPOPPassword() {
		return this.poppassword;
	}
	
	public final String getDisplayName() {
		return this.name;
	}
	
	public final String getDisplayUser() {
		return this.user;
	}
	
	public final String getTextMessage() {
		return this.text;
	}
	
	public final int getSMTPPort() {
		return this.smtpport;
	}
	
	public final int getPOPPort() {
		return this.popport;
	}
	
	public final void setSmtphost(String smtphost) {
		this.smtphost = smtphost;
	}

	public final void setSmtpuser(String smtpuser) {
		this.smtpuser = smtpuser;
	}

	public final void setSmtppassword(String smtppassword) {
		this.smtppassword = smtppassword;
	}

	public final void setPophost(String pophost) {
		this.pophost = pophost;
	}

	public final void setPopuser(String popuser) {
		this.popuser = popuser;
	}

	public final void setPoppassword(String poppassword) {
		this.poppassword = poppassword;
	}

	public final void setName(String name) {
		this.name = name;
	}

	public final void setUser(String user) {
		this.user = user;
	}

	public final void setText(String text) {
		this.text = text;
	}

	public final void setSmtpport(int smtpport) {
		this.smtpport = smtpport;
	}

	public final void setPopport(int popport) {
		this.popport = popport;
	}
}