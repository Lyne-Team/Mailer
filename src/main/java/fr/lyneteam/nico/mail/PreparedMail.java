package fr.lyneteam.nico.mail;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.mail.HtmlEmail;

import fr.lyneteam.nico.mail.MailDataError.Responce;

public class PreparedMail {
	private final List<String> addresses;
	private final List<Mail> messages;
	
	public PreparedMail() {
		this.addresses = new ArrayList<String>();
		this.messages = new ArrayList<Mail>();
	}
	
	public void addAddresses(String... addresses) {
		for (String address : addresses) this.addresses.add(address);
	}
	
	public void removeAddresses(String... addresses) {
		for (String address : addresses) this.addresses.remove(address);
	}
	
	public void removeAddresses(int... addresses) {
		for (int address : addresses) this.addresses.remove(address);
	}
	
	public final List<String> getAddresses() {
		return new ArrayList<String>(this.addresses);
	}
	
	public void addMessages(Mail... messages) {
		for (Mail message : messages) this.messages.add(message);
	}
	
	public void removeMessages(Mail... messages) {
		for (Mail message : messages) this.messages.remove(message);
	}
	
	public void removeMessages(int... messages) {
		for (int message : messages) this.messages.remove(message);
	}
	
	public final List<Mail> getMessages() {
		return new ArrayList<Mail>(this.messages);
	}
	
	public boolean send(MailServerConfiguration configuration) throws MailDataError {
		for (Mail mail : this.messages) {
			for (String address : this.addresses) {
				HtmlEmail email = new HtmlEmail();
				email.setSSL(false);
				email.setHostName(configuration.getSMTPHost());
				email.setSmtpPort(configuration.getSMTPPort()); // 25
				email.setPopBeforeSmtp(true, configuration.getPOPHost(), configuration.getPOPUser(), configuration.getPOPPassword());
				email.setSmtpPort(configuration.getPOPPort()); // 587
				email.setAuthentication(configuration.getSMTPUser(), configuration.getSMTPPassword());
				try {
					String message = "";
					for (String line : mail.getLines()) message = message + line;
					email.addTo(address);
					email.setFrom(configuration.getDisplayUser(), configuration.getDisplayName());
					email.setSubject(mail.getSubject());
					email.setHtmlMsg(message);
					email.setTextMsg(configuration.getTextMessage());
					email.send();
					return true;
				} catch (Exception exception) {
					throw new MailDataError(Responce.INTERNAL_ERROR, exception);
				}
			}
		}
		return false;
	}
}