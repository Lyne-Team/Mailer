package fr.lyneteam.nico.mail;

public class MailDataError extends Exception {
	private static final long serialVersionUID = 287804450359775868L;
	private final Responce responce;
	private final Exception exception;
	
	public MailDataError(Responce responce, Exception exception) {
		super(responce.toString());
		this.responce = responce;
		this.exception = exception;
	}
	
	public MailDataError(Responce responce) {
		this(responce, null);
	}
	
	public MailDataError(Exception exception) {
		this(Responce.INTERNET_ERROR, exception);
	}

	public final Responce getResponce() {
		return this.responce;
	}
	
	public final Exception getException() {
		if (this.exception == null) return this; else return this.exception;
	}
	
	public enum Responce {
		INTERNET_ERROR(-99, "Could not connect to the Lyne-Team mail proxy."),
		INTERNAL_ERROR(-98, "Error, use .getException()."),
		DATA_NUMBERS_ERROR(0, "No address sended."),
		DATA_MESSAGES_ERROR(1, "No messages sended."),
		ACCOUNT_ERROR(2, "Not connected to a Lyne-Team account."),
		ACCESS_DENIED(3, "Access denied."),
		SUBSCRIPTION_ERROR(4, "Subscription ended."),
		SMS_SERVER_CONNECTION_ERROR(5, "Mail server connection error."),
		SMS_SERVER_INTERNAL_ERROR(6, "Mail server internal error.");
		
		public static final Responce getResponceByID(int id) {
			for (Responce responce : values()) if (responce.id == id) return responce;
			return null;
		}
		
		private final int id;
		private final String message;
		
		private Responce(int id, String message) {
			this.id = id;
			this.message = message;
		}
		
		@Override
		public String toString() {
			return this.message;
		}
	}
}