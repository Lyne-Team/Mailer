package fr.lyneteam.nico.mail;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import fr.lyneteam.nico.accounts.LyneTeamAccount;
import fr.lyneteam.nico.mail.MailDataError.Responce;

public class PreparedModifiableMail extends PreparedMail {
	public PreparedModifiableMail() {
		super();
	}
	
	@Override
	public boolean send(MailServerConfiguration configuration) throws MailDataError {
		return this.send(configuration, null);
	}
	
	protected final boolean send(MailServerConfiguration configuration, LyneTeamAccount account) throws MailDataError {
		try {
			URL url = new URL("https://lyneteam.eu/mail.php?id=" + account.getID() + "&ip=" + account.getAuthClass().getIP());
			String addresses = "";
			for (String mail : this.getAddresses()) addresses = addresses + ";" + mail;
			addresses = addresses.substring(1);
			String messages = "";
			for (Mail sms : this.getMessages()) if (!sms.getLines().isEmpty()) {
				String message = "";
				for (String line : sms.getLines()) message = message + "<line>" + line;
				message = message.substring(6);
				messages = messages + "<message>" + message;
			}
			messages = messages.substring(9);
			String post = "addresses=" + URLEncoder.encode(addresses, "UTF-8") + "&messages=" + URLEncoder.encode(messages, "UTF-8");
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(post);
			writer.flush();
			try {
				writer.close();
			} catch (Exception exception) {
				// Ignore
			}
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String responce = reader.readLine();
			try {
				reader.close();
			} catch (Exception exception) {
				// Ignore
			}
			try {
				int id = Integer.parseInt(responce);
				if (id == 8) return true;
				throw new MailDataError(Responce.getResponceByID(id));
			} catch (MailDataError exception) {
				throw exception;
			}
		} catch (MailDataError exception) {
			throw exception;
		} catch (Exception exception) {
			exception.printStackTrace();
			throw new MailDataError(exception);
		}
	}
}