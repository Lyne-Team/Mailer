package fr.lyneteam.nico.mail;

import java.util.ArrayList;
import java.util.List;

public class Mail {
	private final List<String> lines;
	private String subject;
	
	public Mail(String subject, String... lines) {
		this.lines = new ArrayList<String>();
		this.subject = subject;
		this.addLines(lines);
	}
	
	public Mail() {
		this(null);
	}
	
	public void addLines(String... lines) {
		for (String line : lines) this.lines.add(line);
	}
	
	public void removeLines(String... lines) {
		for (String line : lines) this.lines.remove(line);
	}
	
	public void removeLines(int... lines) {
		for (int line : lines) this.lines.remove(line);
	}
	
	public final String getLine(int line) {
		return this.lines.get(line);
	}
	
	public final List<String> getLines() {
		return this.lines;
	}
	
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public final String getSubject() {
		return this.subject;
	}
}